# Audio Recorder

🇬🇧: Audio Recorder allows users to record, stop, playback and download audio directly from their web browser. With a simple and easy to use user interface. Made in HTML, CSS and JavaScript

The project uses the MediaStream Recording API, ensuring broad compatibility with most web browsers.

---

🇪🇸: Audio Recorder permite a los usuarios grabar, detener, reproducir y descargar el audio directamente desde su navegador web. Con una interfaz de usuario simple y fácil de usar. Hecho en HTML, CSS y JavaScript

El proyecto utiliza la API de MediaStream Recording, lo que garantiza una compatibilidad amplia con la mayoría de los navegadores web.

---

v1.1