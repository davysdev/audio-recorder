/*
 ____                        
|  _ \  __ ___   ___   _ ___ 
| | | |/ _` \ \ / / | | / __|
| |_| | (_| |\ V /| |_| \__ \
|____/ \__,_| \_/  \__, |___/
                   |___/     
*/
/*
var start = document.getElementById("start-btn");
var stop = document.getElementById("stop-btn");
var play = document.getElementById("play-btn");
var download = document.getElementById("download-btn");

start.addEventListener("click", startRecording);
stop.addEventListener("click", stopRecording);
play.addEventListener("click", playRecording);
download.addEventListener("click", downloadRecording);

const constraints = { audio: true };
let chunks = [];
let mediaRecorder;
let audioURL;

document.getElementById("start-btn").disabled = false;
document.getElementById("stop-btn").disabled = true;
document.getElementById("play-btn").disabled = true;
document.getElementById("download-btn").disabled = true;

function startRecording() {
  navigator.mediaDevices.getUserMedia(constraints)
    .then(stream => {
      mediaRecorder = new MediaRecorder(stream);
      mediaRecorder.start();

      document.getElementById("start-btn").disabled = true;
      document.getElementById("stop-btn").disabled = false;
      document.getElementById("recording-status").style.display = "block";

      mediaRecorder.ondataavailable = function(event) {
        chunks.push(event.data);
      };

      mediaRecorder.onstop = function() {
        const blob = new Blob(chunks, { type: 'audio/ogg; codecs=opus' });
        chunks = [];

        audioURL = URL.createObjectURL(blob);
        document.getElementById("play-btn").disabled = false;
        document.getElementById("download-btn").disabled = false;
        document.getElementById("recording-status").innerHTML = "";
        document.getElementById("stop-btn").disabled = true;
        document.getElementById("start-btn").disabled = false;
      };
    })
    .catch(err => {
      console.log("Error: " + err);
    });
}

function stopRecording() {
  mediaRecorder.stop();
}

function playRecording() {
  const audio = new Audio(audioURL);
  audio.play();
  document.getElementById("play-btn").disabled = true;

  document.getElementById("play-status").style.display = "block";
  
  audio.addEventListener("ended", function() {
    document.getElementById("play-btn").disabled = false;
    document.getElementById("play-status").style.display = "none";
  });
}

function downloadRecording() {
  const link = document.createElement('a');
  link.href = audioURL;
  link.download = 'recording.ogg';
  document.body.appendChild(link);
  link.click();
}*/

  var start = document.getElementById("start-btn");
  var stop = document.getElementById("stop-btn");
  var play = document.getElementById("play-btn");
  var download = document.getElementById("download-btn");
  var timer = document.getElementById("timer");

  start.addEventListener("click", startRecording);
  stop.addEventListener("click", stopRecording);
  play.addEventListener("click", playRecording);
  download.addEventListener("click", downloadRecording);

  const constraints = { audio: true };
  let chunks = [];
  let mediaRecorder;
  let audioURL;
  let timerInterval;
  let seconds = 0;

  document.getElementById("start-btn").disabled = false;
  document.getElementById("stop-btn").disabled = true;
  document.getElementById("play-btn").disabled = true;
  document.getElementById("download-btn").disabled = true;
  timer.textContent = "00:00";

  function startRecording() {
    seconds = 0; // reiniciar el contador
    navigator.mediaDevices.getUserMedia(constraints)
      .then(stream => {
        mediaRecorder = new MediaRecorder(stream);
        mediaRecorder.start();

        document.getElementById("start-btn").disabled = true;
        document.getElementById("stop-btn").disabled = false;
        document.getElementById("recording-status").style.display = "block";

        mediaRecorder.ondataavailable = function(event) {
          chunks.push(event.data);
        };

        mediaRecorder.onstop = function() {
          clearInterval(timerInterval);
          const blob = new Blob(chunks, { type: 'audio/ogg; codecs=opus' });
          chunks = [];

          audioURL = URL.createObjectURL(blob);
          document.getElementById("play-btn").disabled = false;
          document.getElementById("download-btn").disabled = false;
          document.getElementById("recording-status").style.display = "none";
        };
        
        timerInterval = setInterval(updateTimerInterval, 1000);
      })
      .catch(error => {
        console.error(error);
      });
  }

  function stopRecording() {
    mediaRecorder.stop();
    document.getElementById("start-btn").disabled = false;
    document.getElementById("stop-btn").disabled = true;
    document.getElementById("recording-status").style.display = "none";
  }

  function playRecording() {
    const audio = new Audio(audioURL);
    audio.play();
    document.getElementById("play-status").style.display = "block";

    audio.onended = function() {
      document.getElementById("play-status").style.display = "none";
    };
  }

  function downloadRecording() {
    const element = document.createElement("a");
    element.href = audioURL;
    element.download = "recording.ogg";
    element.click();
  }

  function updateTimerInterval() {
    seconds++;
    timer.textContent = formatTime(seconds);
  }

  function formatTime(seconds) {
    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds % 60;
    const minutesDisplay = minutes < 10 ? "0" + minutes : minutes;
    const secondsDisplay = remainingSeconds < 10 ? "0" + remainingSeconds : remainingSeconds;
    return `${minutesDisplay}:${secondsDisplay}`;
  }